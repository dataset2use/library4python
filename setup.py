
from setuptools import setup, find_packages

setup(
    name='dataset2use',
    version='0.1',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='An example python package',
    long_description=open('README.txt').read(),
    install_requires=[
        #'numpy',
    ],
    url='https://github.com/cypher2use/library4python',
    author='TAYAA Med Amine',
    author_email='tayamino@gmail.com'
)
